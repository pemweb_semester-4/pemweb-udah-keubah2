<?php
session_start();

// Periksa sesi dan peran pengguna
if (!isset($_SESSION['NIM']) || $_SESSION['jabatan'] !== 'kepala_departemen') {
    header('Location: login.php');
    exit();
}

?>
<html>

<head>
    <style>
        body{
            margin: 0;
            padding: 0;
            font-family: 'Poppins', sans-serif;
            background-color: #f9f9f9;
        }
        
        th,
        td {
            border: 1px solid;
            width: 800px;
        }
        tr{
            height: 50px;
        }
        tr td:first-child{
            width: 200px;
        }
        table{
            width: 80%;
            margin: 0 auto;
            text-align: center;
            border-collapse: collapse;
        }
        .header{
            text-align: center;
            margin-top: 50px;
            margin-bottom: 20px;
        }
        .button{
            width: 120px;
            height: 30px;
            padding: 5px;
            background-color: #5d9ee4;
            color: #fff;
            cursor: pointer;
            font-size: 16px;
            border: none;
            border-radius: 4px;
        }
    </style>
</head>

<body>
    <div class="header">
        <h1>Daftar Program Kerja BEM</h1>
        <form action="tambahProker.html" method="post" >
            <input type="submit" value="Tambah Proker" name="TambahProker" class="button">
        </form>
    </div>
    

    <table>
        <thead>
            <tr>
                <td><b>No</td>
                <td><b>Nama Program Kerja</td>
                <td><b>Surat Keterangan</td>
                <td><b>Action 1</td>
                <td><b>Action 2</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proker[0] as $prokerKey): ?>
                <tr>
                    <td>
                        <?= $prokerKey["nomorProgram"] ?>
                    </td>
                    <td>
                        <?= $prokerKey["namaProgram"] ?>
                    </td>
                    <td>
                        <?= $prokerKey["suratKeterangan"] ?>
                    </td>
                    <td> 
                        <form action="updateProker.php"method="get">
                            <input type="submit" value="Update" name="UpdateProker" class="button">
                        </form>   
                    </td>
                    <td>
                        <form action="index.php" method="get">
                            <input type="hidden" name="namaProker" value="<?= $prokerKey['namaProgram'] ?>">
                            <input type="submit" value="Delete" name="DeleteProker" class="button">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>