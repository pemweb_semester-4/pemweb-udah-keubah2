<?php
session_start();
require_once("m_pengurusBEM.php");

if (isset($_POST["Login"])) {
    $nim = $_POST["NIM"];
    $password = $_POST["password"];

    $pengurus = pengurusBEM::getDetail($nim, $password);

    if(!empty($pengurus)){
        $_SESSION["NIM"] = $pengurus[0]["nim"];
        $_SESSION["nama"] = $pengurus[0]["nama"];
        $_SESSION["jabatan"] = $pengurus[0]["jabatan"];
    }

    if($_SESSION["jabatan"] == "Kepala Departemen"){
        header("Location: v_programKerja.php");
    }elseif($_SESSION["jabatan"] == "Menteri"){
        header("Location: v_programKerja2.php");
    } else{
        header("Location: login.php");
    }
    exit();

} else{
    header("Location: login.php");
    exit();
}


?>