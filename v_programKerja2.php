

<html>

<head>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: 'Poppins', sans-serif;
            background-color: #f9f9f9;
        }

        th,
        td {
            border: 1px solid;
            width: 800px;
        }

        tr {
            height: 50px;
        }

        tr td:first-child {
            width: 200px;
        }

        table {
            width: 80%;
            margin: 0 auto;
            text-align: center;
            border-collapse: collapse;
        }

        .header {
            text-align: center;
            margin-top: 50px;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>
    <h1>Daftar Program Kerja BEM</h1>
    <table>
        <thead>
            <tr>
                <td><b>No</td>
                <td><b>Nama Program Kerja</td>
                <td><b>Surat Keterangan</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proker[0] as $prokerKey): ?>
                <tr>
                    <td>
                        <?= $prokerKey["nomorProgram"] ?>
                    </td>
                    <td>
                        <?= $prokerKey["namaProgram"] ?>
                    </td>
                    <td>
                        <?= $prokerKey["suratKeterangan"] ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>