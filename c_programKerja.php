<?php
include_once ("m_programKerja.php");

class c_programKerja
{
    public $model;

    public function __construct()
    {
        $this->model = new m_programKerja();
    }

    public function createProker($nomorProgram, $namaProgram, $suratKeterangan)
    {
        $this->model->setPogramKerja($nomorProgram, $namaProgram, $suratKeterangan);
    }

    public function updateProker ($nomorProgram, $namaProgram, $suratKeterangan){
        $this->model->updateProgramKerja($nomorProgram, $namaProgram, $suratKeterangan);
    }
    
    public function deleteProker($namaProgram)
    {
        $this->model->deletePogramKerja($namaProgram);
    }

    public function showProker()
    {
        $proker = m_programKerja::getSemuaPogramKerja();
        require 'v_programKerja.php';
    }

    public function viewProker($jabatan, $nama){
        $proker = m_programKerja::getSemuaPogramKerja();
        echo "Halo" . $jabatan . " " . $nama;
        if ($jabatan == "Kepala Departemen") {
            require 'v_programKerja.php';
            } else {
            require 'v_programKerja2.php';
            }
            
    }
}