
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="login-container">
        <h1>Login</h1>
        <h4>Selamat datang, silahkan login</h4>
        <form action="c_Login.php" method="POST">
            <p>NIM</p>
            <input type="text" id="NIM" name="NIM" class="input-field" placeholder="NIM" required>
            <p>Password</p>
            <input type="password" name="password" class="input-field" placeholder="Password" required>
            <p><button type="submit" name="Login" class="login">Login</button></p>
            
        </form>
    </div>
</body>
</html>